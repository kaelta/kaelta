<table>
  <tr width="30%">
  <a href="https://www.nataziel.nexus"><h1>Kae 🤍</h1></a>
  <a href="https://thin-different-mine.glitch.me"> all my links :globe: </a>
  <br/>
  <b>tech stack:</b>
  <br/>
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-plain.svg" height="30" width="42" alt="never gonna"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/denojs/denojs-original.svg" height="30" width="42" alt="give you"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nextjs/nextjs-original.svg" height="30" width="42" alt="up never"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jamstack/jamstack-original.svg" height="30" width="42" alt="gonna let"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/redis/redis-original.svg" height="30" width="42" alt="you down"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/postgresql/postgresql-original.svg" height="30" width="42" alt="never gonna"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/rust/rust-original.svg" height="30" width="42" alt="run around"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-original.svg" height="30" width="42" alt="and hurt"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/debian/debian-original.svg" height="30" width="42" alt="you"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nixos/nixos-original.svg" height="30" width="42" alt="(except this one)"/>
  </tr>

  <h2>Personal Projects :gear:</h2>
  
  <tr width="70%">
    <td> <li>🌐 <a href="https://nataziel.nexus">Kaeforest</a> - My personal website</li></td>
    <td><li>♟️ <a href="https://gitlab.com/kaelta/genghis">Genghis</a> -Multithreaded (chess bot) process utility tool. Connects and runs bots on  <a href="lichess.org">Lichess.</li></td>
    <td><li>🌐 <a href="https://img2conv.onrender.com">img2conv</a> - Fast, small and free image conversion tool.</li></td>
    <td> <li>🌐 <a href="https://gitlab.com/kaelta/celeste">Celeste</a> - A cute lil RCE bot to exploit for your CTFs :)</li></td>
  </tr>
</table>

<p>
  artist @ <a href="https://kaexmachina.bandcamp.com"> bandcamp :) </a>
</p>
